import Vue from 'vue'
import App from './App.vue'
import axios from 'axios';


Vue.config.productionTip = false
const data = {time:'wayne'}
var vm = new Vue({
  data: data,
  render: h => h(App),
  mounted() {
    axios.get(`https://dm9l11s9q5.execute-api.ap-southeast-2.amazonaws.com/Prod/hello/`)
    .then(function(response){
      // JSON responses are automatically parsed.
      vm.time = response
    })
    .catch(function(){
    })
  }
}).$mount('#app')
